*** Settings ***
Documentation    1.1 VSR Dream Car|Login
Force Tags    Automatisiert
Test Timeout    5 minute 42 seconds
Library    Browser

*** Test Cases ***
1.1.1.1 Chromium as browser
    [Documentation]    DEFAULT-TC-97488-PC-421069
    #Called by new IA: ----01_Fachlich.Starte Applikation----
    Open Browser    https://vsr.testbench.com/login    chromium    False
    Get Title    ==    VirtualShowRoom II
    #Called by new IA: ----01_Fachlich.Login with valid User----
    Fill Text    id=input_username    Demo01_c01
    Fill Secret    id=input_password    password_c01
    Click    id=button_login
    Browser.Take Screenshot     c:/temp/demo
    Get Title    ==    VirtualShowRoom II
    Get Url    ==    https://vsr.testbench.com/dreamcar/list
    #End of called IA: -----------------------------------------------------
    [Teardown]    Run Keyword    Close Browser


1.1.1.2 Firefox as browser
    [Documentation]    DEFAULT-TC-97488-PC-421213
    #Called by new IA: ----01_Fachlich.Starte Applikation----
    Open Browser    https://vsr.testbench.com/login    firefox    False
    Get Title    ==    VirtualShowRoom II
    #Called by new IA: ----01_Fachlich.Login with valid User----
    Fill Text    id=input_username    Demo01_c01
    Fill Secret    id=input_password    password_c01
    Click    id=button_login
    Browser.Take Screenshot     c:/temp/demo
    Get Title    ==    VirtualShowRoom II
    Get Url    ==    https://vsr.testbench.com/dreamcar/list
    #End of called IA: -----------------------------------------------------
    [Teardown]    Run Keyword    Close Browser

1.1.1.3 Webkit as browser
    [Documentation]    DEFAULT-TC-97488-PC-421214
    #Called by new IA: ----01_Fachlich.Starte Applikation----
    Open Browser    https://vsr.testbench.com/login    webkit    False
    Get Title    ==    VirtualShowRoom II
    #Called by new IA: ----01_Fachlich.Login with valid User----
    Fill Text    id=input_username    Demo01_c01
    Fill Secret    id=input_password    password_c01
    Click    id=button_login
    Browser.Take Screenshot     c:/temp/demo
    Get Title    ==    VirtualShowRoom II
    Get Url    ==    https://vsr.testbench.com/dreamcar/list
    #End of called IA: -----------------------------------------------------
    [Teardown]    Run Keyword    Close Browser
