'use strict';

var readdir = require('promise').denodeify(require('fs').readdir),
    robot = require('robotremote'),
    assert = require('assert'),
    newman = require('newman');

var lib = module.exports;
var failureMessages;
var errorMessage;
var execution;
var htmlReportFile = '../Results/requestReport';
var htmlReportFileExtra = '../Results/requestReportExtra';
var htmlReport;

/**
* Example of asynchronous keyword.
*
* You can implement asynchronous keywords just returning an A+ promise.
* Promise can be resolved or rejected with respectively:
*
* - arbitrary return value, or
* - an instance of `Error` if the keyword failed
*
* Just count items in given directory.
*
* @param path directory path to count item in.
*/
lib.countItemsInDirectory = function (path) {
    return readdir(path).then(function (files) {
        return files.length;
    });
};
// The doc attribute is used for inspection on the command line of client and doc generation.
// It's optional and defaults to empty string when missing.
lib.countItemsInDirectory.doc = 'Returns the number of items in the directory specified by `path`.';

/**
* Example synchronous keyword.
*
* Any keyword which does not return an A+ promise is considered sync.
* The following are considered successes:
*
* - the keyword returns `undefined` (that is doesn't return any value)
* - the keyword return any other value
*
* While any thrown `Error` instance will lead the keyword failure.
*
* Each keyword also have the output writer, which enables logging at various levels.
* Here warn level is showed as an example.
* All robot levels are supported including messages with timestamp through timestamp`Level` function.
* See http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#logging-information
*
* @param str1
* @param str2
*/
lib.checkNewmanVersion = function (str1, str2) {
    //https://developer.mozilla.org/en-US/docs/Web/API/Console
    //this.output.debug('Comparing \'%s\' to \'%s\'', str1, str2);
    this.output.debug('Ths dummy function is used to check if Remote Newman Server is running!');
    assert.equal(str1, str2, 'Given strings are not equal');
};

lib.runCollection = function (collection, environment){
console.log('collection: %s', collection);
console.log('environment: %s', environment);
console.log('htmlReport: %s', htmlReportFile);
htmlReport = htmlReportFile;
    const result = new Promise((resolve, reject) => {
      newman.run({
        collection: collection,
        insecure: true,
        abortOnError:true,
        environment: environment,
        reporters: ['cli','html'],
        reporter: {
          html: {
            export: htmlReport + '_' + Date.now() + '.html', // If not specified, the file will be written to `newman/` in the current working directory.
                  }
        }
      }, function () {
        //console.log('in callback')
        console.info('collection run complete!')
      }).on('start', function (err, args) {
        if (err) { console.log(err) }
      }).on('beforeDone', function (err, data) {
        if (err) { console.log(err) }
      }).on('done', function (err, summary) {
        if (err) { reject(err) } else {
           failureMessages = summary.run.failures,
           errorMessage = summary.error,
           execution = summary.run.executions,
           resolve(summary.run.stats)}
      })
    });
    return result
}

lib.runFolder = function (collection, folderId, environment){
console.log('collection: %s', collection);
console.log('folder: %s', folderId);
console.log('environment: %s', environment);
console.log('htmlReport: %s',htmlReportFileExtra);
console.log ('Date: %s', Date.now());
let timestamp = Date.now();
console.log(new Date(timestamp));
htmlReport = htmlReportFileExtra;
    const result = new Promise((resolve, reject) => {
      newman.run({
        collection: collection,
        folder: folderId,
        insecure: true,
        environment: environment,
        reporters: ['cli','htmlextra'],
        reporter: {
          htmlextra: {
            export: htmlReport + '_' + Date.now() + '.html', // If not specified, the file will be written to `newman/` in the current working directory.
            //template: './customTemplate.hbs' // optional, this will be picked up relative to the directory that Newman runs in.
          }
        }
      }, function () {
        //console.log('in callback')
        console.info('collection xyz run complete!')
      }).on('start', function (err, args) {
        if (err) { console.log(err) }
      }).on('beforeDone', function (err, data) {
        if (err) { console.log(err) }
      }).on('done', function (err, summary) {
        if (err) { reject(err) } else {
           failureMessages = summary.run.failures,
           errorMessage = summary.error,
           execution = summary.run.executions,
           //console.log('execution: %s', execution);
           resolve(summary.run.stats)}
      })
    });
    return result
}

lib.getFailureMessages = function(){
   return failureMessages;
}

lib.getExecutionDetails = function(){
   return execution;
}

lib.getLinkToHtmlReport = function(){
   return htmlReport;
}

lib.setHtmlReportFilename = function(arg1){
   htmlReportFile = arg1;
}

lib.runNewman_bak = function (arg1, arg2){
console.log(arg1);
console.log(arg2);
    return new Promise((resolve, reject) => {
      newman.run({
        collection: arg1,
        insecure: true,
        reporters: 'cli',
        //environment: require(this.environment + '.postman_environment.json')
      }, function () {
        console.log('in callback')
        console.info('collection run complete!')
      }).on('start', function (err, args) {
        if (err) { console.log(err) }
      }).on('beforeDone', function (err, data) {
        if (err) { console.log(err) }
      }).on('done', function (err, summary) {
        if (err) { reject(err) } else { resolve(summary) }
      })
    });
}




// Run this keyword library if the library itself is called explicitly.
if (!module.parent) {
    var server = new robot.Server([lib], { host: 'localhost', port: 8270 });
}

// Run this keyword library if the library itself is called explicitly.
//if (!module.parent) {
//    var robot = require('../lib/robotremote');
//    var options = {
//        host: process.argv[2] || 'localhost',
//        port: parseInt(process.argv[3], 10) || 8270,
//        timeout: 2000,
//        allowStop: true
//    };
//    var server = new robot.Server([lib], options);
//}


