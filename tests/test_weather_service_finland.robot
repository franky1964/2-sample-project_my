*** Settings ***
Resource    ../resources/weather_service_finland.resource
#Test Setup    Open Browser    https://en.ilmatieteenlaitos.fi/    headlessfirefox
Test Setup    Open Browser    https://en.ilmatieteenlaitos.fi/     headlesschrome
Test Teardown    Close Browser

*** Test Cases ***
Test opening weather page for Helsinki
    When Open weatherpage for Helsinki
    Then Is selected observation station 'Helsinki Kaisaniemi'

Test Get current temperature from Helsinki
    ${current_temperature}    When Get current temperature from Helsinki
    Then Should be a Number    ${current_temperature}

*** Keywords ***
Is selected observation station '${station}'
    Page Should Contain Element    xpath://select[@id='observation-station-menu' and @name='station']//option[@selected='selected' and text()='Helsinki Kaisaniemi']    
    Capture Element Screenshot    xpath://section[@aria-labelledby="observations"]

Should Be a Number
    [Arguments]    ${number}
    Should Match Regexp    ${number}    \\d*\\.\\d{1}
