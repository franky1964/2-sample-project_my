*** Settings ***
Suite Setup      Check Remote Server
Resource    ../resources/postman.resource

*** Variables ***
#${COLLECTION-UID}    12328044-a3472de9-4586-4235-9cc1-a2ba1c62b154
${COLLECTION-UID-TESTBENCH}    12328044-5a7d4893-107d-4ae4-bac8-b76173066275   #TestBench
${COLLECTION-FILE-TESTBENCH}   ../Postman_Collections/TestBench.postman_collection.json
#${COLLECTION-UID}    d184b916-b5fa-4478-b16b-1d343eb98064   #Demo IFP
${OPTION_LOCAL}   ../Postman_Environments/MyLocalEnviroment.json
${OPTION_REMOTE}   ../Postman_Environments/MyRemoteEnviroment.json
#${OPTIONS_2}    -r junit,htmlextra,allure --reporter-junit-export junit.xml --reporter-htmlextra-export results.html
${FOLDER_UID}    ba9db46a-1b13-44ed-a6c2-561eb94b3096   #TestBench - ServerVersion - GetVersion
${FOLDER_NAME}    ServerVersion

*** Keywords ***
Check Remote Server
    Check Newman Version    abc   abc

*** Test Cases ***
Run TestBench Collection by UID (local)
    [Tags]    TestBench    Local    UID
    ${myResponse}=    Start Postman Collection    ${COLLECTION-UID-TESTBENCH}    ${OPTION_LOCAL}
    Should be equal    ${myResponse['databaseVersion']}    2.6.01

Run TestBench Collection by File (local)
    [Tags]    TestBench    Local    File
    ${myResponse}=    Start Postman Collection    ${COLLECTION-FILE-TESTBENCH}    ${OPTION_LOCAL}
    Should be equal    ${myResponse['version']}    2.6.01

Run Named Folder of TestBench Collection (local)
    [Tags]    TestBench    Local    UID   Folder
   ${myResponse}=    Start Postman Folder    ${COLLECTION-FILE-TESTBENCH}    ${FOLDER_NAME}    ${OPTION_LOCAL}
    ${version}=     Extract Item Of Response    ${myResponse}    databaseVersion
    Should be equal    ${version}    2.6.01

Select And Run Postman Collection (local)
    [Tags]    Local    Select    Collection
    ${collection}=   Select Postman Collection AS URL
    ${myResponse}=    Start Postman Collection    ${collection}    ${OPTION_LOCAL}
    Should be equal    ${myResponse['version']}    2.6.01

Select Environment And Run Postman Collection
    [Tags]    Select   Environment   Collection
    ${environment}=    Select Postman Environment As URL
    ${collection}=    Select Postman Collection AS URL
    ${myResponse}=    Start Postman Collection    ${collection}    ${environment}
    Should be equal    ${myResponse['version']}    2.6.01

Select Environment And Run Folder Of Postman Collection
    [Tags]    Try    Select    Environment    Collection    Folder
    ${environment}=    Select Postman Environment As URL
    ${collection}=    Select Postman Collection As URL
    ${folder}=   Select Folder of Postman Collection    ${COLLECTION-UID-TESTBENCH}
    ${myResponse}=    Start Postman Folder    ${collection}    ${folder}    ${environment}
    Log    ${myResponse}
    #Should be equal    ${myResponse['version']}    2.6.01
